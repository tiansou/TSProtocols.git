//
//  TS_ViewProtocol.h
//  TS_Component
//
//  Created by TsouMac2016 on 2018/12/13.
//  Copyright © 2018 TsouMac2016. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol TS_ViewModelProtocol;

NS_ASSUME_NONNULL_BEGIN

@protocol TS_ViewProtocol <NSObject>

@optional
/**
 * 试图遵守协议，内部实现与viewmodel的绑定
 */
- (instancetype)initWithViewModel:(id <TS_ViewModelProtocol>)viewModel;
/**
 * 绑定信号(view内操作)
 */
-(void)bindingSignal_view;
/**
 * 添加子视图
 */
-(void)addSubviews_view;

@end

NS_ASSUME_NONNULL_END
