//
//  TS_ViewControllerProtocol.h
//  TS_Component
//
//  Created by TsouMac2016 on 2018/12/13.
//  Copyright © 2018 TsouMac2016. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol TS_ViewControllerProtocol <NSObject>

@optional
/**
 * 绑定信号(controller内操作)
 */
-(void)bindingSignal_viewController;
/**
 * 添加子视图
 */
-(void)addSubviews_viewController;
/**
 * 更新导航栏
 */
-(void)layoutNavigation;
/**
 * 更新数据
 */
-(void)loadNewData;

@end

NS_ASSUME_NONNULL_END
