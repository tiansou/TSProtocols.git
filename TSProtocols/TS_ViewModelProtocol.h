//
//  TS_ViewModelProtocol.h
//  TS_Component
//
//  Created by TsouMac2016 on 2018/12/13.
//  Copyright © 2018 TsouMac2016. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol TS_ViewModelProtocol <NSObject>

@optional
/**
 * 初始化数据
 */
-(void)initializeViewModel;

@end

NS_ASSUME_NONNULL_END
